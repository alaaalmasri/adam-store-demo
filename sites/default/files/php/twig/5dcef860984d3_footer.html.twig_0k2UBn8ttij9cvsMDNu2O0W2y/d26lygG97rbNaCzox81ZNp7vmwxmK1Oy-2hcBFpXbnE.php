<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/AdamStore/footer.html.twig */
class __TwigTemplate_fdc6306e8e1cda8498a339e5a61f90a48c027ef475f4104e246c11fdb55e7e5b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 32];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "    <section>
        <div class=\"subscribe\">
            <div class=\"container\">
                <div class=\"subscribe-content\">
                    <div class=\"row\">
                        <div class=\"col-md-3 col-sm-4 col-xs-12\">
                            <h5>اشترك فى نشرتنا الاخبارية</h5>

                        </div>
                        <div class=\"col-md-4 col-sm-4 col-xs-12\">
                            <div class=\"subscribe-text\">
                                <p>احصل على اخر التحديثات على المنتجات الجديدة والمبيعات القادمة</p>

                            </div>

                        </div>
                        <div class=\"col-md-5 col-sm-4 col-xs-12\">
                            <input type=\"text\" class=\"form-control\" placeholder=\"عنوان بريدك الالكترونى\">
                            <div class=\"subscribe-button\">
                                <button class=\"btn\">اشترك</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> 
       <footer>
        <div class=\"row\">
            <div class=\"col-md-3 col-sm-12 ccol-xs-12\">
                <div class=\"footer-logo\">
                    <img src=\"";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "themes/AdamStore/assets/images/footerlogo.png\" class=\"img-responsive\">
                </div>
            </div>
            <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"footer-links1\">
                    ";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_contact2", [])), "html", null, true);
        echo "
                </div>
            </div>
                <div class=\"col-md-3 col-sm-12 col-xs-12\">
                <div class=\"footer-links1\">
                  ";
        // line 42
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_contact1", [])), "html", null, true);
        echo "
                </div>
            </div>
            <div class=\"col-md-3 col-sm-3 col-xs-12\">
                <div class=\"contact-icons\">
         ";
        // line 47
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_social_media", [])), "html", null, true);
        echo "

             

                </div>
            </div>
        </div>
<hr class=\"footer-hr\">
   <div class=\"copyrights\">
      <div class=\"container\">
          <div class=\"mastercard\">
           <img src=\"";
        // line 58
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "themes/AdamStore/assets/images/visalogo.png\" class=\"img-responsive\">
           <p>2019 All Rights Reserved Terms of Use and Privacy Policy</p>
       </div>
       
      </div>
       
   </div>
    </footer>

";
    }

    public function getTemplateName()
    {
        return "themes/AdamStore/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 58,  112 => 47,  104 => 42,  96 => 37,  88 => 32,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/AdamStore/footer.html.twig", "C:\\wamp64\\www\\AdamStore\\themes\\AdamStore\\footer.html.twig");
    }
}
