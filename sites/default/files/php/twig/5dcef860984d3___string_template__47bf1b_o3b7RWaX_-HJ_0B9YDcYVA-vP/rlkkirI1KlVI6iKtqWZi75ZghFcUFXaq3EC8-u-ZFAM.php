<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__47bf1bf8228c9ee0366b06c7ac7644565b06b413830d1656066b65abc704d6ec */
class __TwigTemplate_66abfd2d0ab7eed97f4ffa17d6afcee97d449a7ee55fe6cc32289dee4be5b69e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 3];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "    <div class=\"col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"offers-images\">
";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_offer1"] ?? null)), "html", null, true);
        echo "
                   </div>
                </div>
                <div class=\"second-row\">
<div class=\"row\">
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    <div class=\"offers-images\">
                  ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_offer2"] ?? null)), "html", null, true);
        echo "
                    </div>
</div>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    <div class=\"offers-images\">
                   ";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_offer3"] ?? null)), "html", null, true);
        echo " 
                    </div>
                </div>
</div>
   </div>";
    }

    public function getTemplateName()
    {
        return "__string_template__47bf1bf8228c9ee0366b06c7ac7644565b06b413830d1656066b65abc704d6ec";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 15,  69 => 10,  59 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__47bf1bf8228c9ee0366b06c7ac7644565b06b413830d1656066b65abc704d6ec", "");
    }
}
