<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/AdamStore/header.html.twig */
class __TwigTemplate_1183af01d8014053c05816b6c22ddbe44c6080b06de10ec243b85fe282a56cd5 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = [];
        $filters = ["escape" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
    <button id=\"top-btn\"><i class=\"fas fa-arrow-up\"></i></button>
    <header>
        <div class=\"top-header\">
            <div class=\"row\">


                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    <div class=\"contact\">
                     ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "contact", [])), "html", null, true);
        echo "
                    </div>
                </div>
                <div class=\"col-md-6 col-sm-6 col-xs-12\">
                    <div class=\"login-btn\">

                        <a href=\"#\">تسجيل دخول</a>
                    
                        <button id=\"eng\"><p style=\"direction: ltr\">ENGLISH</p></button>    
                        
                    </div>
                </div>

            </div>
        </div>
    </header>
    <div class=\"main-header\">
        <div class=\"row\">
            <div class=\"col-md-2 col-sm-12 col-xs-12\">
                <div class=\"logo\">
                    <img src=\"";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null)), "html", null, true);
        echo "themes/AdamStore/assets/images/logo.png\" class=\"img-responsive\">
                </div>
            </div>
            <div class=\"col-md-8 col-sm-8 col-xs-12\">
                <div class=\"main-nav\">
                    <nav class=\"navbar navbar-expand-lg navbar-light\">
                        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                            <span class=\"navbar-toggler-icon\"></span>
                        </button>

                        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                     ";
        // line 41
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_navigation", [])), "html", null, true);
        echo "
                        </div>
                    </nav>
                </div>
            </div>
            <div class=\"col-md-2 cols-sm-12 col-xs-12\">
                <div class=\"search-section\">
                    <i class=\"fa fa-search\"> </i>

                    <input type=\"text\" class=\"form-control search\" placeholder=\"بحث\">

                </div>

            </div>

        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "themes/AdamStore/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 41,  89 => 30,  66 => 10,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/AdamStore/header.html.twig", "C:\\wamp64\\www\\AdamStore\\themes\\AdamStore\\header.html.twig");
    }
}
