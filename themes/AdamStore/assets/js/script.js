 jQuery(document).ready(function($) {
     
     $(window).on("scroll",function() {
        if ($(this).scrollTop() > 50 ) {
            $('#top-btn').fadeIn();
        } else {
            $('#top-btn').fadeOut();
        }
    });

    $("#top-btn").on("click",function() {
        $("html, body").animate({scrollTop: 0}, 800);
     });
    });
